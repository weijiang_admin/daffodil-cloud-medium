package com.daffodil.medium.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.daffodil.medium.properties.MediumProperties;
import com.daffodil.medium.properties.TencentyunProperties;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;

/**
 * 
 * @author yweijian
 * @date 2022年11月11日
 * @version 1.0
 * @description
 */
@Component
@Configuration
@ConditionalOnProperty(name = "daffodil.medium.sender", havingValue = "tencentyun")
public class TencentSmsClientConfig {

    @Autowired
    private MediumProperties mediumProperties;

    @Bean
    @ConditionalOnMissingBean
    public SmsClient smsClient() {
        TencentyunProperties properties = mediumProperties.getTencentyun();

        Credential credential = new Credential(properties.getSecretId(), properties.getSecretKey());
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setReqMethod("POST");
        httpProfile.setConnTimeout(30);
        httpProfile.setEndpoint(properties.getEndpoint());

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        SmsClient client = new SmsClient(credential, properties.getRegion(), clientProfile);
        return client;
    }
}
