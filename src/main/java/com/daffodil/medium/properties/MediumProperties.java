package com.daffodil.medium.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
@ConfigurationProperties(prefix = "daffodil.medium")
public class MediumProperties {
	
	/**
	 * -短信媒介发送类型：aliyun、tencentyun
	 */
	private String sender;

	private EmailProperties email;
	
	private AliyunProperties aliyun;
	
	private TencentyunProperties tencentyun;
}
