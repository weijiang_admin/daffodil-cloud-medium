package com.daffodil.medium.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * -阿里云短信服务配置
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class AliyunProperties {

	public final static String SENDER = "aliyun";

	/** 区域ID */
	private String region;

	/** 产品域名 */
	private String endpoint;
	
	/** 产品accessKeyId */
	private String accessKeyId;
	
	/** 产品accessKeySecret */
	private String accessKeySecret;
	
	/** 短信签名 */
	private String signName;
	
}
