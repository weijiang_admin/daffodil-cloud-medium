package com.daffodil.medium.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * -腾讯云短信服务配置
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class TencentyunProperties {

	public final static String SENDER = "tencentyun";
	
	/** 区域ID */
	private String region;
	
	/** 产品域名 */
	private String endpoint;
	
	/** 产品secretId */
	private String secretId;
	
	/** 产品secretKey */
	private String secretKey;
	
	/** 产品sdkAppId */
	private String sdkAppId;
	
	/** 短信签名 */
	private String signName;
}
