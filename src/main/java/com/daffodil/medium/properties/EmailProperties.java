package com.daffodil.medium.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * -邮件服务配置
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class EmailProperties {
	
	public final static String SENDER = "email";
	
    /** 邮件域名 */
    private String host;
    
    /** 邮件端口 */
    private Integer port;
    
    /** 是否认证 */
    private Boolean auth;
    
    /** 传输协议 */
    private String protocol;
    
    /** 邮件账号 */
    private String user;
    
    /** 邮件授权码 */
    private String password;
    
}
