package com.daffodil.medium.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.medium.entity.SysMediumLog;

/**
 * 
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
public interface ISysMediumLogService {
	
    /**
     * -分页查询讯息信息集合
     * @param query
     * @return
     */
    public List<SysMediumLog> selectMediumLogList(Query<SysMediumLog> query);

    /**
     * -通过讯息ID查询讯息信息
     * @param id
     * @return
     */
    public SysMediumLog selectMediumLogById(String id);

    /**
     * -批量删除讯息信息
     * @param ids
     */
    public void deleteMediumLogByIds(String[] ids);

    /**
     * -新增保存讯息信息
     * @param mediumLog
     */
    public void insertMediumLog(SysMediumLog mediumLog);

}
