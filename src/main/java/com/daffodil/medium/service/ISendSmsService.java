package com.daffodil.medium.service;

import java.util.Map;

/**
 * -发送短信讯息接口服务
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
public interface ISendSmsService {

	/**
	 * -讯息发送
	 * @param paramMap Map属性参数
	 */
	public void send(Map<String, String> paramMap);
}
