package com.daffodil.medium.service;

/**
 * -发送邮件讯息接口服务
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
public interface ISendEmailService {

	/**
	 * -讯息发送
	 * @param account 接收账号
	 * @param subject 发送主题
	 * @param content 消息内容
	 */
	public void send(String account, String subject, String content);
}
