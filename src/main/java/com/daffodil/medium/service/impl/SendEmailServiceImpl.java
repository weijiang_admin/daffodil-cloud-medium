package com.daffodil.medium.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daffodil.framework.exception.BaseException;
import com.daffodil.medium.entity.SysMediumLog;
import com.daffodil.medium.properties.EmailProperties;
import com.daffodil.medium.properties.MediumProperties;
import com.daffodil.medium.service.ISendEmailService;
import com.daffodil.medium.service.ISysMediumLogService;
import com.daffodil.util.JacksonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Slf4j
@Service
public class SendEmailServiceImpl implements ISendEmailService {

    @Autowired
    private ISysMediumLogService mediumLogService;

    @Autowired
    private MediumProperties mediumProperties;

    @Override
    public void send(String account, String subject, String content) {

        EmailProperties email = mediumProperties.getEmail();
        Properties properties = new Properties();
        properties.put("mail.smtp.host", email.getHost());
        properties.put("mail.transport.protocol", email.getProtocol());
        properties.put("mail.smtp.auth", email.getAuth());
        Session session = Session.getInstance(properties);

        SysMediumLog mediumLog = new SysMediumLog();
        mediumLog.setAccount(account);
        mediumLog.setSubject(subject);
        mediumLog.setContent(content);
        mediumLog.setSender(EmailProperties.SENDER);
        mediumLog.setCreateTime(new Date());

        try (Transport transport = session.getTransport()){
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email.getUser()));
            List<InternetAddress> addresses = new ArrayList<InternetAddress>();
            addresses.add(new InternetAddress(account));
            message.addRecipients(Message.RecipientType.TO, addresses.toArray(new InternetAddress[1]));
            message.setSubject(subject);
            message.setContent(content, "text/html;charset=UTF-8");
            transport.connect(email.getHost(), email.getPort(), email.getUser(), email.getPassword());
            transport.sendMessage(message, message.getAllRecipients());

            mediumLog.setMessage("邮件发送成功");
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }

        } catch (MessagingException e) {
            log.error(e.getMessage(), e);
            mediumLog.setMessage(e.getMessage());
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }
            throw new BaseException(e.getMessage());
        }
    }

}
