package com.daffodil.medium.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.medium.entity.SysMediumLog;
import com.daffodil.medium.service.ISysMediumLogService;

/**
 * 
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Service
public class SysMediumLogServiceImpl implements ISysMediumLogService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysMediumLog> selectMediumLogList(Query<SysMediumLog> query) {
        StringBuffer hql = new StringBuffer("from SysMediumLog where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysMediumLog.class, query.getPage());
    }

    @Override
    public SysMediumLog selectMediumLogById(String id) {
        return jpaDao.find(SysMediumLog.class, id);
    }

    @Override
    @Transactional
    public void deleteMediumLogByIds(String[] ids) {
        jpaDao.delete(SysMediumLog.class, ids);
    }

    @Override
    @Transactional
    public void insertMediumLog(SysMediumLog mediumLog) {
        jpaDao.save(mediumLog);
    }

}
