package com.daffodil.medium.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.daffodil.framework.exception.BaseException;
import com.daffodil.medium.entity.SysMediumLog;
import com.daffodil.medium.properties.MediumProperties;
import com.daffodil.medium.properties.TencentyunProperties;
import com.daffodil.medium.service.ISendSmsService;
import com.daffodil.medium.service.ISysMediumLogService;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * -腾讯云短信发送服务
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Slf4j
@Service
@Configuration
@ConditionalOnProperty(name = "daffodil.medium.sender", havingValue = "tencentyun")
public class SendSmsTencentyunServiceImpl implements ISendSmsService {

    @Autowired
    private ISysMediumLogService mediumLogService;

    @Autowired
    private MediumProperties mediumProperties;

    @Autowired
    private SmsClient client;

    /**
     * -讯息发送
     * @param paramMap <br>
     * { "account" : "18088888888", "subject" : "发送主题", "content" : "讯息内容", "templateId" : "模板ID", "templateParam" : "[ "张三", "1234" ]" }
     */
    @Override
    public void send(Map<String, String> paramMap) {
        TencentyunProperties properties = mediumProperties.getTencentyun();

        SendSmsRequest request = new SendSmsRequest();
        request.setSmsSdkAppId(properties.getSdkAppId());
        request.setSignName(properties.getSignName());
        request.setTemplateId(paramMap.get("templateId"));
        String templateParam = paramMap.get("templateParam");
        if(StringUtils.isNotEmpty(templateParam)) {
            List<?> templateParams = JacksonUtils.toJavaObject(templateParam, List.class);
            if(StringUtils.isNotEmpty(templateParams)) {
                request.setTemplateParamSet(templateParams.toArray(new String[templateParams.size()]));
            }
        }
        String[] phoneNumbers = {"+86" + paramMap.get("account")};
        request.setPhoneNumberSet(phoneNumbers);

        SysMediumLog mediumLog = new SysMediumLog();
        mediumLog.setAccount(paramMap.get("account"));
        mediumLog.setSubject(paramMap.get("subject"));
        mediumLog.setContent(paramMap.get("content"));
        mediumLog.setSender(TencentyunProperties.SENDER);
        mediumLog.setCreateTime(new Date());

        try {
            SendSmsResponse response = client.SendSms(request);
            mediumLog.setMessage(SendSmsResponse.toJsonString(response));
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }
        } catch (TencentCloudSDKException e) {
            log.error(e.getMessage(), e);
            mediumLog.setMessage(e.getMessage());
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }
            throw new BaseException(e.getMessage());
        }
    }

}
