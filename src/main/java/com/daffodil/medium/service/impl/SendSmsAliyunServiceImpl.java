package com.daffodil.medium.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.medium.entity.SysMediumLog;
import com.daffodil.medium.properties.AliyunProperties;
import com.daffodil.medium.properties.MediumProperties;
import com.daffodil.medium.service.ISendSmsService;
import com.daffodil.medium.service.ISysMediumLogService;
import com.daffodil.util.JacksonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * -阿里云短信发送服务
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Slf4j
@Service
@Configuration
@ConditionalOnProperty(name = "daffodil.medium.sender", havingValue = "aliyun")
public class SendSmsAliyunServiceImpl implements ISendSmsService {

    @Autowired
    private ISysMediumLogService mediumLogService;

    @Autowired
    private MediumProperties mediumProperties;

    @Autowired
    private AsyncClient client;

    /**
     * -讯息发送
     * @param paramMap <br>
     * { "account" : "18088888888", "subject" : "发送主题", "content" : "讯息内容", "templateId" : "模板ID", "templateParam" : "{ "name" : "张三", "number" : "1234" }" }
     */
    @Override
    public void send(Map<String, String> paramMap) {
        AliyunProperties properties = mediumProperties.getAliyun();

        SendSmsRequest request = SendSmsRequest.builder()
                .resourceOwnerId(1L)
                .phoneNumbers(paramMap.get("account"))
                .signName(properties.getSignName())
                .templateCode(paramMap.get("templateId"))
                .templateParam(paramMap.get("templateParam"))
                .build();

        CompletableFuture<SendSmsResponse> response = client.sendSms(request);
        SysMediumLog mediumLog = new SysMediumLog();
        mediumLog.setAccount(paramMap.get("account"));
        mediumLog.setSubject(paramMap.get("subject"));
        mediumLog.setContent(paramMap.get("content"));
        mediumLog.setSender(AliyunProperties.SENDER);
        mediumLog.setCreateTime(new Date());

        try {
            SendSmsResponse resp = response.get();
            mediumLog.setMessage(resp.toMap().toString());
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage(), e);
            mediumLog.setMessage(e.getMessage());
            mediumLogService.insertMediumLog(mediumLog);
            if(log.isInfoEnabled()) {
                log.info(JacksonUtils.toJSONString(mediumLog));
            }
            throw new BaseException(e.getMessage());
        }
    }
}
