package com.daffodil.medium.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -讯息发送日志
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_medium_log")
public class SysMediumLog extends BaseEntity<String> {

    private static final long serialVersionUID = 8267056529464653350L;

    /** 日志编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 发送类型 */
    @Column(name = "sender", length = 32)
    private String sender;

    /** 接收账号 */
    @Column(name = "account", length = 64)
    private String account;

    /** 信息主题 */
    @Column(name = "subject", length = 256)
    private String subject;

    /** 信息内容 */
    @Column(name = "content", length = 1024)
    private String content;

    /** 发送状态（0正常 1异常） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_success_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 发送结果 */
    @Column(name = "message", length = 2048)
    private String message;

    /** 发送时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;

}
