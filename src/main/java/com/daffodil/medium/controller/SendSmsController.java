package com.daffodil.medium.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.medium.constant.MediumConstant;
import com.daffodil.medium.service.ISendSmsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -短信发送管理<br>
 * -仅限内部服务调用
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Api(value = "短信发送管理", tags = "短信发送管理")
@RestController
@RequestMapping(MediumConstant.API_CONTENT_PATH)
public class SendSmsController {

    @Autowired
    private ISendSmsService sendSmsService;

    @ApiOperation("发送短信讯息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "account", value = "邮箱账号", paramType = "body", required = true),
            @ApiImplicitParam(name = "subject", value = "发送主题", paramType = "body", required = false),
            @ApiImplicitParam(name = "content", value = "讯息内容", paramType = "body", required = false),
            @ApiImplicitParam(name = "templateId", value = "模板标识", paramType = "body", required = false),
            @ApiImplicitParam(name = "templateParam", value = "模板参数", paramType = "body", required = false, 
            examples = @Example(value = {
                    @ExampleProperty(value = "{ \"name\" : \"张三\", \"number\" : \"1234\" }"),
                    @ExampleProperty(value = "[ \"张三\", \"1234\" ]")
            }))
    })
    @PostMapping("/sms/send")
    public Mono<JsonResult> send(@RequestBody Map<String, String> paramMap, @ApiIgnore ServerHttpRequest request) {
        sendSmsService.send(paramMap);
        return Mono.just(JsonResult.success());
    }

}
