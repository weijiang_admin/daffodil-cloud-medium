package com.daffodil.medium.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.medium.constant.MediumConstant;
import com.daffodil.medium.service.ISendEmailService;
import com.daffodil.util.JacksonUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -邮件发送管理<br>
 * -仅限内部服务调用
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
@Api(value = "邮件发送管理", tags = "邮件发送管理")
@RestController
@RequestMapping(MediumConstant.API_CONTENT_PATH)
public class SendEmailController {

    @Autowired
    private ISendEmailService sendEmailService;

    @ApiOperation("发送邮件讯息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "account", value = "邮箱账号", paramType = "body", required = true),
            @ApiImplicitParam(name = "subject", value = "发送主题", paramType = "body", required = true),
            @ApiImplicitParam(name = "content", value = "讯息内容", paramType = "body", required = true)
    })
    @PostMapping("/email/send")
    public Mono<JsonResult> send(@RequestBody Map<String, Object> paramMap, @ApiIgnore ServerHttpRequest request) {
        String account = JacksonUtils.getAsString(paramMap, "account");
        String subject = JacksonUtils.getAsString(paramMap, "subject");
        String content = JacksonUtils.getAsString(paramMap, "content");
        sendEmailService.send(account , subject, content);
        return Mono.just(JsonResult.success());
    }

}
