package com.daffodil.medium.constant;

/**
 * 
 * @author yweijian
 * @date 2022年11月9日
 * @version 1.0
 * @description
 */
public class MediumConstant {

	/** api上下文地址 */
    public static final String API_CONTENT_PATH = "/api-medium";
}
